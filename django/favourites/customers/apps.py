from django.apps import AppConfig


class CustomersApiConfig(AppConfig):
    name = 'customer_api'

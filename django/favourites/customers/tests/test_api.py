import json

import pytest
from rest_framework import status


@pytest.mark.django_db
def test_should_return_empty_list_of_customers(client, url_customers):
    response = client.get(url_customers)
    response_data = json.loads(response.content)

    assert response.status_code == status.HTTP_200_OK
    assert len(response_data) == 0


@pytest.mark.django_db
def test_should_list_all_customers(client, url_customers, customer):
    response = client.get(url_customers)

    assert response.status_code == status.HTTP_200_OK
    assert response.data[0]['name'] == 'Shaquille O\'Neal'
    assert len(response.data) == 1


@pytest.mark.django_db
def test_should_save_customer(client, url_customers):
    payload = {
        'name': 'Mr. Jones',
        'email': 'iwannabebobdylan@wheneverybodyloveyou.com'
    }

    response = client.post(url_customers, data=payload)

    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_should_patch_customer(client, url_customers, customer):
    payload = {
        'name': 'Mr. Jones'
    }

    url_customers = f'{url_customers}{customer.id}/'
    response = client.patch(url_customers, data=payload, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['name'] == payload['name']
    assert response.data['name'] != customer.name


@pytest.mark.django_db
def test_should_put_customer(client, url_customers, customer):
    payload = {
        'name': 'Mr. Jones',
        'email': 'itsoktobehardcore@gmail.com'
    }

    url_customers = f'{url_customers}{customer.id}/'
    response = client.patch(url_customers, data=payload, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['name'] == payload['name']
    assert response.data['name'] != customer.name


@pytest.mark.django_db
def test_should_delete_customer(client, url_customers, customer):
    url_customers = f'{url_customers}{customer.id}/'
    response = client.delete(url_customers, format='json')

    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.django_db
@pytest.mark.parametrize('param_name, invalid_content', [
    ('name', None,),
    ('email', None,),
    ('email', 'email_invalido.com',)
])
def test_invalid_payload(param_name, invalid_content, client, url_customers):
    payload = {
        'name': 'Mr. Jones',
        'email': 'itsoktobehardcore@gmail.com',
        param_name: invalid_content
    }

    response = client.post(url_customers, data=payload, format='json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_should_get_customer(client, url_customers, customer):
    url_customers = f'{url_customers}{customer.id}/'
    response = client.get(url_customers, format='json')

    assert response.status_code == status.HTTP_200_OK
    assert response.data['id'] == customer.id
    assert response.data['name'] == customer.name
    assert response.data['email'] == customer.email


@pytest.mark.django_db
def test_should_return_404_not_found_customer(client, url_customers):
    url_customers = f'{url_customers}120982193812/'
    response = client.get(url_customers, format='json')

    assert response.status_code == status.HTTP_404_NOT_FOUND

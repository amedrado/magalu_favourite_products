from django.contrib import admin
from favourites.customers.models import Customer

admin.site.register(Customer)

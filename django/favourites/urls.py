from rest_framework.authtoken.views import obtain_auth_token

from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from favourites.customers import views as customers
from favourites.products import views as products

urlpatterns = [
    url(
        r'^api/v1/customers/$',
        customers.CustomerList.as_view(),
        name='list-customer'
    ),
    url(
        r'^api/v1/customers/(?P<pk>[0-9]+)/$',
        customers.CustomerDetail.as_view(),
        name='detail-customer'
    ),
    url(
        r'^api/v1/products/$',
        products.ProductViewset.as_view({'post': 'create'}),
        name='product-create'
    ),
    url(
        r'^api/v1/products/(?P<customer_id>[0-9]+)/$',
        products.ProductViewset.as_view({'get': 'list'}),
        name='product-list'
    ),
    path('admin/', admin.site.urls),
    path('token/', obtain_auth_token, name='api_token_auth'),
]

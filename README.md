# Magalu Recrutamento Interno

### Challenge API URL: http://challenge-api.luizalabs.com

## Instructions
* Create a virtualenv based on python 3.7.4
* Enable venv and install dependencies
* eg: `pyenv activate magalu-favourites`
* `pip install -r django/requirements/development.txt`
    
## Create an admin superuser
* `python django/manage.py createsuperuser --settings=favourites.settings.development`
* Or you can use `make superuser`
* If you like, you can create a user, a group and set Customer and Products permissions
    
## Endpoints and Requests
* All requests are available as a postman collection, you can found it 
at `postman` folder
* When generating a token, you must use the username and password created before
on payload.
    
## Running Tests
* `make test`

## Further informations 
If you like, you can run `docker-compose up` to avoid installing PostgreSQL
and Redis on your machine_

* Running migrations: `make migrate`
